---
comments: false
title: Sobre mim
subtitle: 
---

*Prof. Dr João Batista Tolentino Júnior*

+ Engenheiro Agrônomo pela Universidade Estadual de Maringá - UEM (2006)
Mestre em Produção Vegetal pela Universidade Estadual de Maringá - UEM (2008)
+ Doutor em Irrigação e Drenagem pela Escola Superior de Agricultura Luiz de Queiroz da Universidade de São Paulo - Esalq/USP (2012)
+ Professor Adjunto do Departamento de Agricultura, Biodiversidade e Florestas do Centro de Ciências Rurais da Universidade Federal de Santa Catarina, Campus de Curitibanos - ABF/CCR/UFSC
+ Orientador de mestrado do Programa de Pós-graduação em Ecossistemas Agrícolas e Naturais - PPGEAN/UFSC

