---
date: "2020-03-05"
title: Validar CPF
slug: validar-cpf
subtitle: 
tags:
- r-code
- CPF

---


Função em R para validar o número do CPF brasileiro. :key:

O CPF deve ser fornecido como um string no formato "###.###.###-##"

Depende dos pacotes: 

+ stringr
+ magrittr

Retorna TRUE quando o CPF é válido.

Abaixo o código da função em R:

```r
valida_cpf <- function(x="529.982.247-25") {
  #x = cpf "###.###.###-##"
  
  if (!require(stringr))
    install.packages("stringr")
  library(stringr)
  
  if (!require(magrittr))
    install.packages("magrittr")
  library(magrittr)
  
  #ver1: tem que ser igual ao primeiro digito verificador
  ver1 <-
    paste0(str_sub(x, 1, 3),
           str_sub(x, 5, 7),
           str_sub(x, 9, 11))
  
  ver1t <- str_split(ver1, "") %>%
    unlist %>%
    as.numeric %>%
    multiply_by(10:2) %>%
    sum %>%
    multiply_by(10) %>%
    mod(11)
  
  if (ver1t == 10) ver1t = 0
  
  ver1t <-  ver1t %>%
    equals(as.numeric(str_sub(x, 13, 13)))
  
  #ver 2: tem que ser igual ao segundo digito verificador
  ver2 <-
    paste0(str_sub(x, 1, 3),
           str_sub(x, 5, 7),
           str_sub(x, 9, 11),
           str_sub(x, 13, 13))
  
  ver2t <- str_split(ver2, "") %>%
    unlist %>%
    as.numeric %>%
    multiply_by(11:2) %>%
    sum %>%
    multiply_by(10) %>%
    mod(11)
  
  if (ver2t == 10) ver2t = 0
  
  ver2t <- ver2t %>%
    equals(as.numeric(str_sub(x, 14, 14)))
  
  return(all(ver1t, ver2t))
}
```

