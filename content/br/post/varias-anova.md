---
date: "2021-03-18"
title: Anova para várias variáveis
slug: varias-anova
subtitle: 
tags:
- r-code
- ANOVA

---

Calcular e apresentar a Análise de Variância para várias variáveis

```r
x <- rep(c("T1","T2","T3", "T4", "T5"), e=4)
y <- list(
  A=rnorm(20),
  B=rnorm(20),
  c=rnorm(20)
)

df <- data.frame(x,y)

library(magrittr)

lapply(df[-1], function(y) {lm(y~x) %>% anova()})

``´