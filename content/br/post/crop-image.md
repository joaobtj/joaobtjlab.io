---
date: "2021-03-20"
title: Cortar várias imagens com EBImage
slug: crop-image
subtitle: 
tags:
- r-code
- EBImage

---

Lista todos os arquivos de imagens e corta para as dimensões listadas em ix e iy.

([fonte](https://stackoverflow.com/a/56013886/10315600))


```r
fl <- list.files()
library(EBImage)

for (i in fl) {
  f <- readImage(i)
  # plot(f)
  ix <- 1:768         #horizontal dimension
  iy <- 172:1183      #vertical dimension
  dim(f)              # determine the dimensions
  nf <- f[ix, iy, 1:4]
  # plot(nf)
  writeImage(nf, paste0("crop/", i))
}

```
