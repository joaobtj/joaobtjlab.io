---
date: "2021-02-03"
title: Filtrando linhas com NA
slug: na
subtitle: 
tags:
- r-code
- tidyverse

---

Filtrar as linhas que contém algum NA em qualquer uma das colunas `everything()` ou nas colunas numéricas `where(is.numeric)` 
.
```r
 library(tidyverse)
 
 a <- c("a","b","c")
 b <- c(1,2,3)
 c <- c(4,NA,6)
 d <- c(NA,7,8)
 e <- c(9,NA,11)
 
 df <- data.frame(a,b,c,d,e)

 df %>% filter(across(where(is.numeric), ~ !is.na(.))) %>%
   setdiff(df,.) 
   
```