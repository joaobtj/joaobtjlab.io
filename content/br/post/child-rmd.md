---
date: "2020-02-04"
title: Como inserir recursivamente um arquivo Rmd dentro de outro
slug: child-rmd
subtitle: 
tags:
- r-code
- Rmd

---


Neste exemplo, o arquivo *gg_boxplot-template.Rmd* é chamado para cada variável *y* do data.frame ([fonte](https://github.com/yihui/knitr-examples/blob/master/020-for-loop.Rnw)).


O arquivo principal ([child-rmd.Rmd](https://joaobtj.gitlab.io/files/child-rmd.Rmd)) tem o seguinte conteúdo:

````markdown
```{r data}
set.seed(456)
x <- rep(1:3, e = 6)
y1 <- 1 * x + rnorm(length(x))
y2 <- 2 * x + rnorm(length(x))
y3 <- 3 * x + rnorm(length(x))
dat4 <- data.frame(x, y1, y2, y3)
```

```{r out}
out = NULL
for (i in 1:(dim(dat4)[2] - 1)) {
  datb <- data.frame(x = dat4[, 1], y = dat4[, i + 1])
  out = c(out, knitr::knit_child('_gg_boxplot-template.Rmd'))
}
```

`r paste(knitr::knit(text = out), collapse = '\n')`

````


O arquivo [gg_boxplot-template.Rmd](https://joaobtj.gitlab.io/files/gg_boxplot-template.Rmd) terá o seguinte conteúdo:

```{r, echo=FALSE, results='asis'}

library(ggplot2, quietly = TRUE, warn.conflicts = FALSE)
bp <- ggplot(datb, aes(x = factor(x), y = y)) +
  geom_boxplot()
bp

```




