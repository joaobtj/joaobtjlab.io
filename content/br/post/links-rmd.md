---
date: "2021-03-02"
title: Criar links para todos os arquivos listados em um diretório
slug: lnks-rmd
subtitle: 
tags:
- r-code
- Rmd

---

Criar links dinamicamente para todos os arquivos listados em um diretório. Pode ser utilizada a opção `patch` na função `list.files` para listar os arquivos em um subdiretório. Também pode ser utilizada a opção `pattern` na função `list.files` para restringir a uma extensão.

````md
```{r, include=FALSE}
library(knitr)
# listar os arquivos 
files <- list.files()

# criar vetor com o texto dos links
out = NULL
for (i in files) {
  out = c(out, knit_expand(text='[{{i}}]({{i}}) \n'))
}
```
`r paste(knit(text = out))`
````

Outra opção, criando uma função (´link`) para inserir os links. Esta opção permite mais personalização na construção dos links.


````md

```{r, include=FALSE}
# Função para adicionar link
link <- function(texto, link, inline=TRUE) {
  if(inline) {
    paste("[", texto, "](", link, ")", sep="")
  } else {
    cat("[", texto, "](", link, ")", "<br/>",  sep="")
  }
}
```

```{r,echo=FALSE, results="asis"}
# listar os arquivos na pasta public
files <- list.files("public/")

for (i in files){
  link(i, paste0("public/",i), inline=FALSE)
  }
```

```` 

[Fonte: stackoverflow.com](https://stackoverflow.com/a/52747944/10315600)


