---
{}
---
Olá, bem vindo.
Esta é a minha página pessoal. Aqui compartilho uma parte do meu trabalho.

Na seção [BLOG](/), compartilho códigos curtos e funções em "R" nas áreas de bioestatística, engenharia entre outras, além de ideias sobre assuntos ligados às ciências agrárias e outras áreas de meu interesse.

Na seção [AULAS](aulas/), disponibilizo livros online que são utilizados como apoio às discilinas que ministro na Universidade Federal de Santa Catarina - UFSC Campus de Curitibanos.