---
comments: false
title: Aulas
---




### Hidráulica Agrícola 

*e-book Hidráulica Agrícola*: é um livro de apoio para a disciplina de Hidráulica Agrícola ministrada no curso de Agronomia UFSC/CCR.

[link: Hidráulica Agrícola](https://hidraulica.tolentino.pro.br/)


### Irrigação Pressurizada

*e-book Irrigação Pressurizada*: é um livro de apoio para a disciplina de Irrigação e Drenagem  ministrada no curso de Agronomia UFSC/CCR.

[link: Irrigação Pressurizada](https://irrigacao.tolentino.pro.br/)

### Bioestatística *(em breve)*
