

```{r data}
x <- rep(1:3, e = 6)
y1 <- 1 * x + rnorm(length(x))
y2 <- 2 * x + rnorm(length(x))
y3 <- 3 * x + rnorm(length(x))
datc <- data.frame(x, y1, y2, y3)
```

```{r out}

out = NULL
for (i in 1:(dim(datc)[2] - 1)) {
  datb <- data.frame(x = datc[, 1], y = datc[, i + 1])
  out = c(out, knit_child('gg_boxplot-template.Rmd'))
}
```

`r paste(knit(text = out), collapse = '\n')`








